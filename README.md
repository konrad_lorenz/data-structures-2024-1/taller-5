# Tarea de Curso: Construcción y Manejo de Árboles AVL y Árboles B

## Parte 1: Árboles Binarios de Búsqueda Auto-Balanceables (AVL)

### Objetivo
Comprender y aplicar los principios de inserción, balanceo y eliminación en árboles AVL.

### Instrucciones
Para cada uno de los siguientes conjuntos de números, realiza las siguientes tareas:
- **Conjunto de números 1:** 50, 30, 70, 20, 40, 60, 80, 10, 25, 35, 45, 55, 65, 75, 85, 95
- **Conjunto de números 2:** 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125
- **Conjunto de números 3:** 40, 55, 65, 35, 70, 25, 15, 75, 30, 10, 45, 85, 60, 20, 50, 80, 90, 95

#### Tareas
1. **Inserción y Balanceo:**
   - Inserta los números uno por uno en un árbol AVL.
   - Dibuja el árbol después de cada inserción.
   - Verifica si el árbol está balanceado después de cada inserción. Si no lo está, aplica las rotaciones necesarias para balancearlo y documenta el proceso.
   - Explica brevemente por qué cada rotación fue necesaria.

## Parte 2: Árboles B

### Objetivo
Desarrollar habilidades en la construcción y manejo de árboles B, incluyendo inserciones, divisiones y eliminaciones.

### Instrucciones
Utiliza el **Conjunto de números 2 y 3 ** para construir árboles B, siguiendo estas pautas:

#### Tareas
1. **Construcción de Árboles B:**
   - Inserta los números uno por uno en un árbol B con k=4 y luego con k=5.
   - Documenta cómo se añade cada clave al árbol.
   - Observa y anota cuándo y cómo se divide un nodo durante las inserciones.
   - Dibuja el árbol después de cada inserción y cada división.
   - Calcula y documenta el orden del árbol y el número mínimo de hijos por nodo.

2. **Eliminación de claves:**
   - Elimina las claves 30, 40 y 60 de ambos árboles B.
   - Documenta cómo cada clave es eliminada.
   - Explica cómo se manejan las reducciones de nodos y las redistribuciones de claves.
   - Dibuja el árbol después de cada eliminación.

3. **Recorridos del Árbol:**
   - Realiza recorridos en preorden, inorden y postorden sobre los árboles AVL construidos con el conjunto de números 2 y 3.
   - Documenta el resultado de cada recorrido.

### Entregables
- Diagramas de cada árbol después de cada operación (inserción, balanceo, división y eliminación).
- Un informe que incluya las explicaciones de cada paso, particularmente el balanceo de los árboles AVL y las divisiones en los árboles B.
- Discusión sobre las diferencias observadas en el comportamiento entre los árboles AVL y los árboles B con diferentes órdenes  (Reporte Latex).
